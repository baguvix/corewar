/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/14 21:13:28 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/22 16:17:49 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ASM_H
# define ASM_H

typedef struct	s_program
{
	t_header	header;
	char		*bytecode;
}				t_program;

int				evaluate_file_name(char *fname);
void			write_champ(char *input_file, t_program *prog);
void			syntax_decode(t_prog_line *lines, t_instruction *head);
void			asm_encode(t_instruction *head, t_program *prog);
void			extract_commands(t_prog_line *lines, t_program *prog);
void			init_cmd(t_cmd *cmd, t_program *prog, int flag);
void			lexer(int fd, t_prog_line *head);

#endif
