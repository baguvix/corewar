/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_syntax.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/19 19:39:24 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/22 16:19:30 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ASM_SYNTAX_H
# define ASM_SYNTAX_H

# define NAME_BIT		1
# define COMMENT_BIT	2

# define PROGRAM_SCOPE	1
# define LINE_SCOPE		2

typedef enum				e_syntax
{
	MNEM_EXP, MISS_ARG, BAD_ARG, BAD_OP, BAD_CMD, MNEM_OR_LABEL_EXP,
	DOUBLE_NAME, DOUBLE_COMMENT, MISS_NAME, MISS_COMMENT, UNDEF_LABEL, DD_LABEL,
	UNEXP_TOK, EMPTY_CODE, OK
}							t_syntax;

typedef struct				s_cmd
{
	size_t					n;
	char					*dst;
	t_syntax				err;
	unsigned char			flag;
}							t_cmd;

typedef	union				u_arg
{
	char					*label;
	int						val;
}							t_arg;

typedef struct				s_instruction
{
	char					*label;
	int						pos;
	unsigned char			op_code;
	t_arg_type				arg_type[MAX_ARGS_NUMBER];
	t_arg					arg[MAX_ARGS_NUMBER];
	unsigned char			len;
	unsigned char			encode_args;
	struct s_instruction	*next;
}							t_instruction;

t_syntax					reg_arg(t_instruction *instr, t_token **tok,
									int i, int id);
int							init_instr(t_instruction *instr, t_token **tok);
int							push_back_instr(t_instruction *head,
											t_instruction *new_instr);
t_syntax					op_rule(t_token **tok, t_instruction *instr);
t_syntax					label_rule(t_token **tok, t_instruction *instr);

#endif
