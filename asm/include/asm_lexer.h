/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_lexer.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/14 20:25:05 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/22 17:07:57 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ASM_LEXER_H
# define ASM_LEXER_H

# define ALPHABET 			"abcdefghijklmnopqrstuvwxyz_"
# define ALT_COMMENT_CHAR	';'
# define STR_LITERAL_CHAR	'"'

# define BUFFER_SIZE		COMMENT_LENGTH

typedef	enum				e_tok_type
{
	OP, CMD, REG, LABEL, VAL, DIR_CHAR, STR_LITERAL, SEP, COMMENT, EOI,
	BAD_TOKEN, BAD_STR_LITERAL, BIG_STR_LITERAL, BAD_VAL, BAD_REG, LAST_LF
}							t_tok_type;

typedef	struct				s_token
{
	t_tok_type				type;
	char					*val;
	struct s_token			*next;
}							t_token;

typedef	struct				s_prog_line
{
	char					*raw_line;
	unsigned				raw_line_num;
	t_token					tokens;
	struct s_prog_line		*next;
}							t_prog_line;

t_tok_type					sep_lexem(char **inp, char *buf);
t_tok_type					comment_lexem(char **inp, char *buf);
t_tok_type					op_lexem(char **inp, char *buf);
t_tok_type					val_lexem(char **inp, char *buf);
t_tok_type					label_lexem(char **inp, char *buf);
t_tok_type					reg_lexem(char **inp, char *buf);
t_tok_type					dir_lexem(char **inp, char *buf);
t_tok_type					str_literal_lexem(char **inp, char *buf);
t_tok_type					cmd_lexem(char **inp, char *buf);
int							push_back_token(t_token *head, t_token *new_tok);
int							push_back_prog_line(t_prog_line *head,
												t_prog_line *new_line);

#endif
