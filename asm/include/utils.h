/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/22 16:17:12 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/22 16:17:15 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef UTILS_H
# define UTILS_H

void	le_to_be(int val, char *dst, unsigned char size);
int		label_lookup(t_instruction *head, char *label);
int		calc_pos(char *line, char *err_tok);
int		is_op_match(char *buf);
void	demolish_tokens(t_token *head);
void	demolish_prog_lines(t_prog_line *head);
void	demolish_instructions(t_instruction *head);

#endif
