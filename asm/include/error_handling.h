/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/14 20:36:11 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/15 00:07:58 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERROR_HANDLING_H
# define ERROR_HANDLING_H

# include <stdio.h>
# include <stdlib.h>

void	syntax_error(t_prog_line *line, t_token *tok,
						t_syntax type, int scope);
void	lexer_error(unsigned line_num, char *err_line,
						int err_pos, t_tok_type err_type);
void	shutdown(char *err_str);

#endif
