/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/10 19:57:10 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/19 19:29:33 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <sys/errno.h>
#include <stdlib.h>
#include "libft.h"
#include "get_next_line.h"
#include "ft_printf.h"
#include "op.h"
#include "asm_lexer.h"
#include "asm_syntax.h"
#include "asm.h"
#include "utils.h"
#include "error_handling.h"

int		translate_program(int fd, t_program *prog)
{
	t_prog_line		lines;
	t_instruction	instructions;

	ft_memset(&lines, 0, sizeof(t_prog_line));
	ft_memset(&instructions, 0, sizeof(t_instruction));
	lexer(fd, &lines);
	extract_commands(&lines, prog);
	syntax_decode(&lines, &instructions);
	demolish_prog_lines(&lines);
	asm_encode(&instructions, prog);
	demolish_instructions(&instructions);
	le_to_be(COREWAR_EXEC_MAGIC,
			(char *)&prog->header.magic, sizeof(COREWAR_EXEC_MAGIC));
	return (0);
}

void	usage(void)
{
	ft_printf("Usage:\n\t./asm {champ_code}.s\n");
	exit(EXIT_SUCCESS);
}

int		main(int argc, char **argv)
{
	int			fd;
	t_program	prog;

	if (argc != 2)
		usage();
	errno = 0;
	if (evaluate_file_name(argv[1]))
		usage();
	if ((fd = open(argv[1], O_RDONLY)) < 0)
		shutdown("main()->open()");
	ft_memset(&prog, 0, sizeof(t_program));
	translate_program(fd, &prog);
	if (close(fd) == -1)
		shutdown("main()->close()");
	write_champ(argv[1], &prog);
	free(prog.bytecode);
	exit(EXIT_SUCCESS);
}
