/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   syntax_rules.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/19 19:23:56 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/19 19:24:23 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "op.h"
#include "asm_lexer.h"
#include "asm_syntax.h"
#include "error_handling.h"

t_syntax	parse_arg(int id, int i, t_token **tok, t_instruction *instr)
{
	if ((*tok)->type == REG)
		return (reg_arg(instr, tok, i, id));
	if ((*tok)->type == DIR_CHAR && (g_op_tab[id].arg[i] & T_DIR))
	{
		instr->arg_type[i] |= T_DIR;
		instr->len += g_op_tab[id].d_short ? 0 : IND_SIZE;
		*tok = (*tok)->next;
	}
	else if (g_op_tab[id].arg[i] & T_IND)
		instr->arg_type[i] |= T_IND;
	else
		return (BAD_ARG);
	instr->len += IND_SIZE;
	if ((*tok)->type == LABEL)
	{
		instr->arg_type[i] |= T_LAB;
		if ((instr->arg[i].label = ft_strdup((*tok)->val)) == NULL)
			shutdown("ft_strdup");
	}
	else if ((*tok)->type == VAL)
		instr->arg[i].val = ft_atoi((*tok)->val);
	else
		return (UNEXP_TOK);
	*tok = (*tok)->next;
	return (OK);
}

t_syntax	op_rule(t_token **tok, t_instruction *instr)
{
	int			id;
	int			i;
	t_syntax	rc;

	if ((id = init_instr(instr, tok)) == -1)
		return (BAD_OP);
	i = 0;
	while (i < g_op_tab[id].n_args)
	{
		if (i > 0)
		{
			if ((*tok)->type != SEP)
				return (MISS_ARG);
			*tok = (*tok)->next;
		}
		rc = parse_arg(id, i++, tok, instr);
		if (rc != OK)
			return (rc);
	}
	if ((*tok)->type == EOI || (*tok)->type == COMMENT)
		return (OK);
	return (UNEXP_TOK);
}

t_syntax	label_rule(t_token **tok, t_instruction *instr)
{
	if ((instr->label = ft_strdup((*tok)->val)) == NULL)
		shutdown("ft_strdup");
	*tok = (*tok)->next;
	if ((*tok)->type == OP)
		return (op_rule(tok, instr));
	if ((*tok)->type == EOI || (*tok)->type == COMMENT)
		return (OK);
	return (MNEM_EXP);
}
