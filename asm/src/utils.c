/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/19 19:12:04 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/19 19:29:22 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "op.h"
#include "asm_lexer.h"
#include "asm_syntax.h"

int		is_op_match(char *buf)
{
	unsigned	i;

	i = 0;
	while (g_op_tab[i].op_token != NULL)
		if (!ft_strcmp(g_op_tab[i++].op_token, buf))
			return (i - 1);
	return (-1);
}

int		calc_pos(char *line, char *err_tok)
{
	char	*tmp;
	int		count;
	int		pos;

	if (err_tok == NULL)
		return (ft_strlen(line));
	tmp = ft_strstr(line, err_tok);
	pos = tmp - line;
	count = 0;
	while (line < tmp)
		if (*line++ == '\t')
			++count;
	return (count * 7 + pos);
}

int		label_lookup(t_instruction *head, char *label)
{
	while (head->next)
	{
		head = head->next;
		if (head->label && !ft_strcmp(head->label, label))
			return (head->pos);
	}
	return (-1);
}

void	le_to_be(int val, char *dst, unsigned char size)
{
	unsigned char	i;

	i = 0;
	while (i < size)
	{
		dst[size - i - 1] = (val >> (i * 8)) & 0xFF;
		++i;
	}
}
