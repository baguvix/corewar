/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   syntax_decode.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/19 19:20:57 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/19 19:37:28 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "op.h"
#include "asm_lexer.h"
#include "asm_syntax.h"
#include "asm.h"
#include "utils.h"
#include "error_handling.h"

t_syntax	process_cmd(t_token **tok, t_program *prog, unsigned char *cmds)
{
	t_cmd	cmd;

	if (!ft_strcmp(NAME_CMD_STRING, (*tok)->val))
		init_cmd(&cmd, prog, NAME_BIT);
	else if (!ft_strcmp(COMMENT_CMD_STRING, (*tok)->val))
		init_cmd(&cmd, prog, COMMENT_BIT);
	else
		return (BAD_CMD);
	if (*cmds & cmd.flag)
		return (cmd.err);
	*tok = (*tok)->next;
	if ((*tok)->type != STR_LITERAL)
		return (MISS_ARG);
	ft_strncpy(cmd.dst, (*tok)->val, cmd.n);
	*cmds |= cmd.flag;
	return (OK);
}

void		extract_commands(t_prog_line *lines, t_program *prog)
{
	unsigned char	cmds;
	t_token			*tok;
	t_syntax		rc;

	cmds = 0;
	while (lines->next)
	{
		lines = lines->next;
		tok = lines->tokens.next;
		if (tok->type != CMD)
			continue ;
		rc = process_cmd(&tok, prog, &cmds);
		if (rc != OK)
			syntax_error(lines, tok, rc, LINE_SCOPE);
	}
	if (!(cmds & NAME_BIT))
		syntax_error(NULL, NULL, MISS_NAME, PROGRAM_SCOPE);
	if (!(cmds & COMMENT_BIT))
		syntax_error(NULL, NULL, MISS_COMMENT, PROGRAM_SCOPE);
}

t_syntax	parse_instruction(t_token **tok,
								t_instruction *instr, t_instruction *head)
{
	if ((*tok)->type == LABEL)
	{
		if (label_lookup(head, (*tok)->val) != -1)
			return (DD_LABEL);
		return (label_rule(tok, instr));
	}
	if ((*tok)->type == OP)
		return (op_rule(tok, instr));
	return (MNEM_OR_LABEL_EXP);
}

void		syntax_decode(t_prog_line *lines, t_instruction *head)
{
	t_instruction	instr;
	t_token			*tok;
	t_syntax		rc;
	unsigned int	pos;

	pos = 0;
	while (lines->next)
	{
		lines = lines->next;
		tok = lines->tokens.next;
		if (tok->type == CMD || tok->type == EOI || tok->type == COMMENT)
			continue ;
		ft_memset(&instr, 0, sizeof(t_instruction));
		rc = parse_instruction(&tok, &instr, head);
		instr.pos = pos;
		pos += instr.len;
		if (rc != OK)
			syntax_error(lines, tok, rc, LINE_SCOPE);
		if (push_back_instr(head, &instr))
			shutdown("push_back_instr");
	}
	if (head->next == NULL)
		syntax_error(NULL, NULL, EMPTY_CODE, PROGRAM_SCOPE);
}
