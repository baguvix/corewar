/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_lexer.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/10 19:15:36 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/22 16:22:33 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"
#include "op.h"
#include "asm_lexer.h"
#include "asm_syntax.h"
#include "utils.h"
#include "error_handling.h"

t_tok_type		parse(char **inp, char *buf)
{
	while (**inp == ' ' || **inp == '\t' || (**inp > '\n' && **inp <= '\r'))
		++(*inp);
	if (**inp == LABEL_CHAR && ++(*inp))
		return (label_lexem(inp, buf));
	if (**inp == '-' || ft_isdigit(**inp))
		return (val_lexem(inp, buf));
	if (**inp == 'r' && ++(*inp))
		return (reg_lexem(inp, buf));
	if (**inp == '.')
		return (cmd_lexem(inp, buf));
	if (**inp == DIRECT_CHAR)
		return (dir_lexem(inp, buf));
	if (**inp && ft_strchr(ALPHABET, **inp))
		return (op_lexem(inp, buf));
	if (**inp == COMMENT_CHAR || **inp == ALT_COMMENT_CHAR)
		return (comment_lexem(inp, buf));
	if (**inp == SEPARATOR_CHAR)
		return (sep_lexem(inp, buf));
	if (**inp == STR_LITERAL_CHAR && ++(*inp))
		return (str_literal_lexem(inp, buf));
	return (**inp == '\0' ? EOI : BAD_TOKEN);
}

int				get_token(t_token *tok, char **inp)
{
	char		buf[BUFFER_SIZE + 1];

	tok->val = NULL;
	tok->type = parse(inp, buf);
	if (tok->type != EOI && (tok->val = ft_strdup(buf)) == NULL)
		shutdown("ft_strdup");
	return (0);
}

void			src_to_tokens(t_prog_line *line)
{
	char	*tmp;
	t_token	t;

	tmp = line->raw_line;
	t.type = BAD_TOKEN;
	t.next = NULL;
	while (t.type != EOI)
	{
		get_token(&t, &tmp);
		if (t.type >= BAD_TOKEN)
			lexer_error(line->raw_line_num, line->raw_line, \
						(int)(tmp - line->raw_line - ft_strlen(t.val)), t.type);
		if (push_back_token(&line->tokens, &t))
			shutdown("push_back_token");
	}
}

void			lexer(int fd, t_prog_line *head)
{
	t_prog_line line;
	char		*inp;
	unsigned	i;
	int			rc;

	i = 1;
	while ((rc = get_next_line(fd, &inp)))
	{
		ft_memset(&line, 0, sizeof(t_prog_line));
		line.raw_line = inp;
		line.raw_line_num = i++;
		src_to_tokens(&line);
		if (push_back_prog_line(head, &line))
			shutdown("push_back_prog_line");
		inp = NULL;
	}
	if (rc == 0 && inp)
		lexer_error(i, inp, ft_strlen(inp), LAST_LF);
	if (rc != 0)
		shutdown("get_next_line");
}
