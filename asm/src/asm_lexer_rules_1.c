/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_lexer_rules_1.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/19 19:30:38 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/19 19:31:01 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "op.h"
#include "asm_lexer.h"

t_tok_type	dir_lexem(char **inp, char *buf)
{
	*buf++ = *(*inp)++;
	*buf = '\0';
	return (DIR_CHAR);
}

t_tok_type	cmd_lexem(char **inp, char *buf)
{
	*buf++ = *(*inp)++;
	while (**inp && ft_strchr(ALPHABET, **inp))
		*buf++ = *(*inp)++;
	*buf = '\0';
	return (CMD);
}

t_tok_type	str_literal_lexem(char **inp, char *buf)
{
	int	size;

	size = 0;
	while (**inp && **inp != STR_LITERAL_CHAR && size < BUFFER_SIZE)
		buf[size++] = *(*inp)++;
	buf[size] = '\0';
	if (**inp == '\0')
		return (BAD_STR_LITERAL);
	if (size == BUFFER_SIZE && **inp != STR_LITERAL_CHAR)
		return (BIG_STR_LITERAL);
	++(*inp);
	return (STR_LITERAL);
}

t_tok_type	reg_lexem(char **inp, char *buf)
{
	int		r;
	char	*tmp;

	tmp = buf;
	while (ft_isdigit(**inp))
		*buf++ = *(*inp)++;
	*buf = '\0';
	if (**inp && ft_strchr(LABEL_CHARS, **inp))
	{
		*inp -= (buf - tmp) + 1;
		return (op_lexem(inp, tmp));
	}
	r = ft_atoi(tmp);
	if (r < 1 || r > REG_NUMBER)
		return (BAD_REG);
	return (REG);
}

t_tok_type	label_lexem(char **inp, char *buf)
{
	while (**inp && ft_strchr(LABEL_CHARS, **inp))
		*buf++ = *(*inp)++;
	*buf = '\0';
	return (LABEL);
}
