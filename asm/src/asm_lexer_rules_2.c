/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_lexer_rules_2.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/19 19:31:19 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/19 19:33:48 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "op.h"
#include "asm_lexer.h"

t_tok_type	val_lexem(char **inp, char *buf)
{
	if (**inp == '-')
		*buf++ = *(*inp)++;
	*buf = '\0';
	if (!ft_isdigit(**inp))
		return (BAD_VAL);
	while (ft_isdigit(**inp))
		*buf++ = *(*inp)++;
	*buf = '\0';
	if (**inp && ft_strchr(LABEL_CHARS, **inp))
		return (op_lexem(inp, buf));
	return (VAL);
}

t_tok_type	op_lexem(char **inp, char *buf)
{
	char	*tmp;

	tmp = buf;
	while (**inp && ft_strchr(LABEL_CHARS, **inp))
		*tmp++ = *(*inp)++;
	*tmp = '\0';
	if (**inp == LABEL_CHAR && ++(*inp))
		return (LABEL);
	return (OP);
}

t_tok_type	comment_lexem(char **inp, char *buf)
{
	int	size;

	size = 0;
	while (**inp && size < BUFFER_SIZE)
		buf[size++] = *(*inp)++;
	buf[size] = '\0';
	return (COMMENT);
}

t_tok_type	sep_lexem(char **inp, char *buf)
{
	*buf++ = *(*inp)++;
	*buf = '\0';
	return (SEP);
}
