/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_encode.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/19 19:24:38 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/19 19:36:53 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "op.h"
#include "asm_lexer.h"
#include "asm_syntax.h"
#include "asm.h"
#include "utils.h"
#include "error_handling.h"

unsigned char	encode_arg_type(t_arg_type type)
{
	if (type & T_REG)
		return (REG_CODE);
	if (type & T_DIR)
		return (DIR_CODE);
	if (type & T_IND)
		return (IND_CODE);
	return (0);
}

void			encode_arg(unsigned char op_code, t_arg_type type,
							t_arg arg, char **bytecode)
{
	unsigned char	arg_size;

	arg_size = 0;
	if (type & T_REG)
		arg_size = 1;
	if (type & T_IND)
		arg_size = IND_SIZE;
	if (type & T_DIR)
		arg_size = g_op_tab[op_code - 1].d_short ? IND_SIZE : DIR_SIZE;
	le_to_be(arg.val, *bytecode, arg_size);
	*bytecode += arg_size;
}

void			encode_instr(t_instruction *instr, char *bytecode)
{
	int	i;

	*bytecode++ = instr->op_code;
	if (instr->encode_args)
		*bytecode++ = (encode_arg_type(instr->arg_type[0]) << 6) \
						| (encode_arg_type(instr->arg_type[1]) << 4) \
						| (encode_arg_type(instr->arg_type[2]) << 2);
	i = 0;
	while (i < g_op_tab[instr->op_code - 1].n_args)
	{
		encode_arg(instr->op_code, instr->arg_type[i],
					instr->arg[i], &bytecode);
		++i;
	}
}

unsigned int	resolve_labels(t_instruction *head)
{
	int				label_pos;
	unsigned int	cur;
	t_instruction	*instr;
	unsigned char	i;

	cur = 0;
	instr = head;
	while (instr->next)
	{
		instr = instr->next;
		if (instr->len == 0)
			continue ;
		i = -1;
		while (++i < g_op_tab[instr->op_code - 1].n_args)
			if (instr->arg_type[i] & T_LAB)
			{
				label_pos = label_lookup(head, instr->arg[i].label);
				if (label_pos == -1)
					syntax_error(NULL, NULL, UNDEF_LABEL, PROGRAM_SCOPE);
				free(instr->arg[i].label);
				instr->arg[i].val = label_pos - cur;
			}
		cur += instr->len;
	}
	return (cur);
}

void			asm_encode(t_instruction *head, t_program *prog)
{
	unsigned int	cur;

	prog->header.prog_size = resolve_labels(head);
	if ((prog->bytecode = (char *)ft_memalloc(prog->header.prog_size)) == NULL)
		shutdown("asm_encode()->ft_memalloc()");
	cur = 0;
	while (head->next)
	{
		head = head->next;
		if (head->len)
			encode_instr(head, prog->bytecode + cur);
		cur += head->len;
	}
}
