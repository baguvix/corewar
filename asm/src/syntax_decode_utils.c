/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   syntax_decode_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/19 19:18:47 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/19 19:35:58 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "op.h"
#include "asm_lexer.h"
#include "asm_syntax.h"
#include "asm.h"
#include "utils.h"

void		init_cmd(t_cmd *cmd, t_program *prog, int flag)
{
	cmd->flag = flag;
	if (flag == NAME_BIT)
	{
		cmd->dst = prog->header.prog_name;
		cmd->n = PROG_NAME_LENGTH;
		cmd->err = DOUBLE_NAME;
	}
	else
	{
		cmd->dst = prog->header.comment;
		cmd->n = COMMENT_LENGTH;
		cmd->err = DOUBLE_COMMENT;
	}
}

t_syntax	reg_arg(t_instruction *instr, t_token **tok, int i, int id)
{
	if (!(g_op_tab[id].arg[i] & T_REG))
		return (BAD_ARG);
	instr->arg_type[i] |= T_REG;
	instr->arg[i].val = ft_atoi((*tok)->val);
	instr->len += 1;
	*tok = (*tok)->next;
	return (OK);
}

int			init_instr(t_instruction *instr, t_token **tok)
{
	int		id;

	if ((id = is_op_match((*tok)->val)) == -1)
		return (-1);
	instr->op_code = g_op_tab[id].op_code;
	instr->encode_args = g_op_tab[id].encode_args;
	instr->len += 1 + instr->encode_args;
	*tok = (*tok)->next;
	return (id);
}

int			push_back_instr(t_instruction *head, t_instruction *new_instr)
{
	while (head->next)
		head = head->next;
	head->next = (t_instruction *)ft_memalloc(sizeof(t_instruction));
	if (head->next == NULL)
		return (1);
	ft_memcpy(head->next, new_instr, sizeof(t_instruction));
	return (0);
}
