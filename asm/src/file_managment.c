/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_managment.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/19 19:27:25 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/19 19:29:13 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include "libft.h"
#include "ft_printf.h"
#include "op.h"
#include "asm_lexer.h"
#include "asm_syntax.h"
#include "asm.h"
#include "utils.h"
#include "error_handling.h"

int		evaluate_file_name(char *fname)
{
	char	*tmp;

	tmp = ft_strstr(fname, ".s");
	if (tmp == NULL || tmp[2] != '\0')
		return (ft_printf("Error: incorrect filetype: %s\n", fname));
	return (0);
}

int		open_champ_file(char *input_file)
{
	char	champ_file[BUFFER_SIZE];
	int		fd;

	*champ_file = '\0';
	ft_strcat(champ_file, input_file);
	ft_strcpy(ft_strstr(champ_file, ".s"), ".cor");
	ft_printf("Writing champ to %s file...\n", champ_file);
	if ((fd = open(champ_file, O_WRONLY | O_TRUNC | O_CREAT, 420)) < 0)
		shutdown("write_champ()->open_champ_file()->open()");
	return (fd);
}

void	write_champ(char *input_file, t_program *prog)
{
	char			null[REG_SIZE];
	unsigned int	be_size;
	int				fd;
	int				rc;

	fd = open_champ_file(input_file);
	ft_memset(null, 0, REG_SIZE);
	rc = write(fd, &prog->header.magic, sizeof(prog->header.magic));
	if (rc > 0)
		rc = write(fd, prog->header.prog_name, PROG_NAME_LENGTH);
	if (rc > 0)
		rc = write(fd, null, REG_SIZE);
	le_to_be(prog->header.prog_size, (char *)&be_size, sizeof(be_size));
	if (rc > 0)
		rc = write(fd, &be_size, sizeof(be_size));
	if (rc > 0)
		rc = write(fd, prog->header.comment, COMMENT_LENGTH);
	if (rc > 0)
		rc = write(fd, null, REG_SIZE);
	if (rc > 0)
		rc = write(fd, prog->bytecode, prog->header.prog_size);
	if (rc <= 0)
		shutdown("write_champ()->write()");
	if (close(fd) == -1)
		shutdown("write_champ()->close()");
}
