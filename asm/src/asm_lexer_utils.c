/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm_lexer_utils.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/19 19:32:43 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/19 19:34:14 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "op.h"
#include "asm_lexer.h"
#include "asm_syntax.h"
#include "asm.h"
#include "error_handling.h"

int		push_back_token(t_token *head, t_token *new_tok)
{
	while (head->next)
		head = head->next;
	head->next = (t_token *)ft_memalloc(sizeof(t_token));
	if (head->next == NULL)
		return (1);
	ft_memcpy(head->next, new_tok, sizeof(t_token));
	return (0);
}

int		push_back_prog_line(t_prog_line *head, t_prog_line *new_line)
{
	while (head->next)
		head = head->next;
	head->next = (t_prog_line *)ft_memalloc(sizeof(t_prog_line));
	if (head->next == NULL)
		return (1);
	ft_memcpy(head->next, new_line, sizeof(t_prog_line));
	return (0);
}
