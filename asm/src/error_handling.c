/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_handling_1.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/19 19:16:29 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/22 16:21:28 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"
#include "ft_printf.h"
#include "op.h"
#include "asm_lexer.h"
#include "asm_syntax.h"
#include "utils.h"

char	*get_err_descr_helper(t_syntax type)
{
	if (type == MNEM_EXP)
		return ("mnemonic expected");
	if (type == MISS_ARG)
		return ("missing argument");
	if (type == BAD_ARG)
		return ("bad argument");
	if (type == BAD_OP)
		return ("bad operation");
	if (type == BAD_CMD)
		return ("bad command");
	if (type == MNEM_OR_LABEL_EXP)
		return ("mnemonic or label expected");
	if (type == DOUBLE_NAME)
		return ("double '.name' command");
	if (type == DOUBLE_COMMENT)
		return ("double '.comment' command");
	return (NULL);
}

char	*get_err_description(t_syntax type)
{
	char	*err;

	if ((err = get_err_descr_helper(type)) != NULL)
		return (err);
	if (type == MISS_NAME)
		return ("\".name\" command is missing");
	if (type == MISS_COMMENT)
		return ("\".comment\" command is missing");
	if (type == UNDEF_LABEL)
		return ("undefined label");
	if (type == DD_LABEL)
		return ("label defined already");
	if (type == UNEXP_TOK)
		return ("unexpected token");
	if (type == EMPTY_CODE)
		return ("empty code");
	return ("unknown error");
}

void	syntax_error(t_prog_line *line, t_token *tok, t_syntax type, int scope)
{
	char	*err_msg;
	int		pos;

	err_msg = get_err_description(type);
	if (scope == PROGRAM_SCOPE)
		ft_printf("\nSyntax error: %s\n", err_msg);
	if (scope == LINE_SCOPE)
	{
		pos = calc_pos(line->raw_line, tok->val);
		ft_printf("\nSyntax error: %s:\n[%d]:\t%s\n\t %*.c\n",
					err_msg, line->raw_line_num, line->raw_line, pos, '^');
	}
	exit(EXIT_FAILURE);
}

void	lexer_error(unsigned line_num, char *err_line,
					int err_pos, t_tok_type err_type)
{
	char	*description;

	description = "unknown err...\n";
	if (err_type == BAD_STR_LITERAL)
		description = "missing terminating '\"' character";
	if (err_type == BAD_VAL)
		description = "bad value";
	if (err_type == BIG_STR_LITERAL)
		description = "string literal is too big";
	if (err_type == BAD_TOKEN)
		description = "bad token";
	if (err_type == BAD_REG)
		description = "bad register";
	if (err_type == LAST_LF)
		description = "unexpected end of input" \
						"(Perhaps you forgot to end with a newline ?)";
	ft_printf("\nLexical error: %s:\n[%d]:\t%s\n\t%*.c\n", description,
				line_num, err_line, err_pos, '^');
	exit(EXIT_FAILURE);
}

void	shutdown(char *err_str)
{
	perror(err_str);
	exit(EXIT_FAILURE);
}
