/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mem_free.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/19 19:26:30 by orhaegar          #+#    #+#             */
/*   Updated: 2020/10/19 19:26:43 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "op.h"
#include "asm_lexer.h"
#include "asm_syntax.h"

void	demolish_tokens(t_token *head)
{
	t_token	*tmp;

	head = head->next;
	while (head)
	{
		tmp = head->next;
		if (head->val)
			free(head->val);
		free(head);
		head = tmp;
	}
}

void	demolish_prog_lines(t_prog_line *head)
{
	t_prog_line	*tmp;

	head = head->next;
	while (head)
	{
		tmp = head->next;
		free(head->raw_line);
		demolish_tokens(&head->tokens);
		free(head);
		head = tmp;
	}
}

void	demolish_instructions(t_instruction *head)
{
	t_instruction	*tmp;

	head = head->next;
	while (head)
	{
		tmp = head->next;
		if (head->label)
			free(head->label);
		free(head);
		head = tmp;
	}
}
