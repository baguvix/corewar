/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:40:04 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:40:06 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	long	tmp;
	char	*fresh;
	size_t	len;

	len = 1;
	tmp = n;
	while (n /= 10)
		++len;
	n = tmp;
	if (tmp < 0)
	{
		tmp *= -1;
		++len;
	}
	if (!(fresh = (char*)malloc((len + 1) * sizeof(char))))
		return (NULL);
	fresh[len] = '\0';
	while (len--)
	{
		fresh[len] = tmp % 10 + '0';
		tmp /= 10;
	}
	if (n < 0)
		*fresh = '-';
	return (fresh);
}
