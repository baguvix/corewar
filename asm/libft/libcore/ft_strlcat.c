/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:51:18 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:51:22 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	dst_filling;

	dst_filling = ft_strlen(dst);
	if (size <= dst_filling || !size)
		return (size + ft_strlen(src));
	dst += dst_filling;
	while (*src && (++dst_filling < size))
		*dst++ = *src++;
	*dst = '\0';
	if (dst_filling == size)
		dst_filling += ft_strlen(src) - 1;
	return (dst_filling);
}
