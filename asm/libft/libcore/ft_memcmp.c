/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:43:42 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:43:43 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	unsigned char	*uc_s1;
	unsigned char	*uc_s2;

	if (n == 0)
		return (0);
	uc_s1 = (unsigned char*)(s1);
	uc_s2 = (unsigned char*)(s2);
	while (n--)
		if (*uc_s1++ != *uc_s2++)
			return (*(uc_s1 - 1) - *(uc_s2 - 1));
	return (0);
}
