/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:59:24 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 03:59:26 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	char	*fresh;
	char	*begin;
	char	*end;

	if (s == NULL)
		return (NULL);
	while (*s && (*s == ' ' || *s == '\t' || *s == '\n'))
		++s;
	begin = (char*)s;
	end = begin;
	while (*s)
	{
		if (*s != ' ' && *s != '\t' && *s != '\n')
			end = (char*)s + 1;
		++s;
	}
	if (!(fresh = (char*)malloc(end - begin + 1)))
		return (NULL);
	fresh[end - begin] = '\0';
	ft_strncpy(fresh, begin, end - begin);
	return (fresh);
}
