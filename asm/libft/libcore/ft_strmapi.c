/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 03:52:26 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/01 04:22:25 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s,
			char (*f)(unsigned int, char))
{
	char			*new;
	size_t			len;
	unsigned int	i;

	if (!(s && f))
		return (NULL);
	len = ft_strlen(s);
	if ((new = (char*)malloc(len + 1)) == NULL)
		return (NULL);
	i = -1;
	while (s[++i])
		new[i] = f(i, s[i]);
	new[i] = '\0';
	return (new);
}
