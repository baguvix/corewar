/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_mati.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: orhaegar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/03 16:17:38 by orhaegar          #+#    #+#             */
/*   Updated: 2019/02/03 16:17:40 by orhaegar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_mati	*ft_create_mati(size_t m, size_t n)
{
	t_mati	*new;

	if (!m || !n)
		return (NULL);
	if ((new = (t_mati*)malloc(sizeof(t_mati))))
		if ((new->elem = (int*)malloc(m * n * sizeof(int))))
		{
			ft_bzero(new->elem, m * n * sizeof(int));
			new->m = m;
			new->n = n;
			return (new);
		}
	return (NULL);
}
