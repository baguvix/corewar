/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_print.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/10 18:25:41 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/10 18:40:32 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_PRINT_H
# define FT_PRINTF_PRINT_H

# include "ft_printf.h"
# include "libft.h"

int		print_to_fd(int fd, t_list *spec);
int		print_to_str(char *res_str, t_list *spec);

#endif
