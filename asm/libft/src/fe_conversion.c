/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fe_conversion.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <rnarbo@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/05 21:33:38 by orhaegar          #+#    #+#             */
/*   Updated: 2019/07/21 22:16:45 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "fp_core.h"
#include "conv.h"

ssize_t		e_conv(t_spec *s, char **rtu)
{
	t_e				e;
	char			*stmp;
	char			*rtmp;
	size_t			len;

	stmp = e_help(&e, (char *)s->value.vp);
	len = (s->prec != 0 ? 1 : 0) + s->prec + (ft_strchr(stmp, '.') ? 1 : 0);
	if ((*rtu = (char *)malloc(len + 5 + nofd(e.pow, 10))) == NULL)
		return (-1);
	correction(stmp, len);
	rtmp = *rtu;
	*rtmp++ = *stmp++;
	if (s->prec != 0)
		*rtmp++ = '.';
	while (s->prec)
		if (*stmp++ != '.' && s->prec--)
			*rtmp++ = *(stmp - 1);
	*rtmp++ = 'e';
	*rtmp++ = e.pow_sign;
	if (e.pow < 10)
		*rtmp++ = '0';
	ft_strcpy(rtmp, stmp = ft_itoa((int)e.pow));
	free(stmp);
	free(s->value.vp);
	return (0);
}

ssize_t		fe_conv(t_spec *s, char **rtu)
{
	char	*raw_str;
	int		tmp;

	s->prec = (s->prec == -1 ? 6 : s->prec);
	tmp = s->prec;
	s->prec = (s->prec == 0 && s->alt ? 1 : s->prec);
	if (ftoa(s, &raw_str) < 0)
		return (-1);
	if (ft_isdigit(*raw_str) && (s->type == 'e' || s->type == 'E'))
	{
		s->value.vp = raw_str;
		raw_str = NULL;
		s->prec = tmp;
		if (e_conv(s, &raw_str) < 0)
			return (-1);
	}
	if (s->alt && tmp == 0)
		*(ft_strchr(raw_str, '.') + 1) = '\0';
	s->value.vp = raw_str;
	return (wrap_up(s, rtu));
}

ssize_t		ftoa(t_spec *s, char **rtu)
{
	int		(*genf)(t_bn *, t_spec *, char **);
	t_bn	num;

	num.a.df.high = 0;
	num.a.f = s->value.ld;
	num.sign = (num.a.df.high & SIGN_OFF) ? '-' : '+';
	num.a.df.high = num.a.df.high & (SIGN_OFF - 1);
	num.pow = num.a.df.high - 63;
	if (num.a.df.high == 0)
		num.pow -= EXP_BIAS_DENORMAL;
	else if (num.a.df.high == POWER_BITS_ALL_ONE)
		return (naninf(&num, s, rtu));
	else
		num.pow -= EXP_BIAS_NORMAL;
	if (s->type == 'e' || s->type == 'E')
		s->prec = ((num.pow + 63 < 0) ?
					(-(num.pow + 63)) / LOG2_10 : 0) + 36 + s->prec;
	genf = (num.pow < 0) ? gen_num_neg : gen_num_nneg;
	if (genf(&num, s, rtu))
		return (-1);
	free(num.p);
	s->sign = (num.sign == '-' ? '-' : s->sign);
	return (0);
}
