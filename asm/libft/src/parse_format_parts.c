/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_format_parts.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/10 19:09:19 by rnarbo            #+#    #+#             */
/*   Updated: 2019/09/15 22:03:23 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "parse_format.h"
#include "libft.h"

const char *g_colors[8] = {
	"black",
	"red",
	"green",
	"yellow",
	"blue",
	"magenta",
	"cyan",
	"white"};

size_t	parse_digit(t_spec *spec, char *format)
{
	size_t	i;
	int		tmp;

	i = 0;
	if (format[i] == '0')
	{
		spec->filling = '0';
		return (1);
	}
	tmp = ft_atoi(format + i);
	while (ft_isdigit(format[i]))
		i++;
	if (format[i] == '$')
	{
		spec->num = tmp;
		i++;
	}
	else
		spec->width = tmp;
	return (i);
}

size_t	parse_flags(t_spec *spec, char *format)
{
	if (!spec || !format)
		return (0);
	if (*format == '-')
		spec->align_left = 1;
	else if (*format == '#')
		spec->alt = 1;
	else if (*format == '+')
		spec->sign = '+';
	else if (*format == ' ')
	{
		if (spec->sign != '+')
			spec->sign = ' ';
	}
	else
		return (0);
	return (1);
}

size_t	parse_length(t_spec *spec, char *format)
{
	char	res;
	int		i;

	i = 1;
	res = 0;
	if (!spec || !format)
		return (1);
	if (ft_strncmp(format, "ll", 2) == 0 && (i = 2) == 2)
		res += 1 << 2;
	else if (ft_strncmp(format, "l", 1) == 0)
		res += 1 << 3;
	if (ft_strncmp(format, "hh", 2) == 0 && (i = 2) == 2)
		res += 1 << 0;
	else if (ft_strncmp(format, "h", 1) == 0)
		res += 1 << 1;
	if (ft_strncmp(format, "L", 1) == 0)
		res += 1 << 4;
	if (ft_strncmp(format, "z", 1) == 0)
		res += 1 << 5;
	if (ft_strncmp(format, "j", 1) == 0)
		res += 1 << 6;
	if (ft_strncmp(format, "t", 1) == 0)
		res += 1 << 7;
	*(char *)&(spec->lenght) |= res;
	return (i);
}

size_t	parse_prec(t_spec *spec, char *format)
{
	int i;

	i = 0;
	if (*format == '*')
	{
		spec->prec = -2;
		return (1);
	}
	spec->prec = ft_atoi(format);
	while (ft_isdigit(format[i]))
		i++;
	return (i);
}

size_t	parse_color(t_spec *spec, char *format)
{
	size_t	len;
	int		j;

	j = 0;
	while (j < 8)
	{
		len = ft_strlen(g_colors[j]);
		if (ft_strncmp(format + 1, g_colors[j++], len) == 0)
		{
			if (*format == '{' && format[len + 1] == '}')
			{
				spec->color = j;
				return (len + 2);
			}
			else if (*format == '[' && format[len + 1] == ']')
			{
				spec->bgcolor = j;
				return (len + 2);
			}
		}
	}
	return (1);
}
