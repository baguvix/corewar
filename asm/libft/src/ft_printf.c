/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rnarbo <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/09 13:56:40 by rnarbo            #+#    #+#             */
/*   Updated: 2019/07/10 18:44:38 by rnarbo           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include "parse_format.h"
#include "ft_printf_print.h"
#include "utils.h"

int	ft_printf(const char *format, ...)
{
	int		res;
	va_list	ap;

	va_start(ap, format);
	res = ft_vdprintf(1, format, ap);
	va_end(ap);
	return (res);
}

int	ft_dprintf(int fd, const char *format, ...)
{
	va_list	ap;
	int		res;

	va_start(ap, format);
	res = ft_vdprintf(fd, format, ap);
	va_end(ap);
	return (res);
}

int	ft_vdprintf(int fd, const char *format, va_list ap)
{
	t_list	*spec;
	int		res;

	spec = 0;
	parse_format(ap, &spec, format);
	res = print_to_fd(fd, spec);
	ft_lstdel(&spec, &del_with_free);
	return (res);
}

int	ft_sprintf(char *str, const char *format, ...)
{
	va_list	ap;
	int		res;

	va_start(ap, format);
	res = ft_vsprintf(str, format, ap);
	va_end(ap);
	return (res);
}

int	ft_vsprintf(char *str, const char *format, va_list ap)
{
	t_list	*spec;
	int		res;

	spec = 0;
	parse_format(ap, &spec, format);
	res = print_to_str(str, spec);
	ft_lstdel(&spec, &del_with_free);
	return (res);
}
